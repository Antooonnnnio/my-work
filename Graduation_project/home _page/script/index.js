$(document).ready(function(){
  $('.slider').slick({
    lazyLoad: 'ondemand',
    slidesToShow: 3,
    slidesToScroll: 1
  });

  $('slider').on('click', function() {
    $('slider').slick('slickNext');
  });
});