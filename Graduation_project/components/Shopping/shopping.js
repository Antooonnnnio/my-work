class Shopping {
    handlerClear() {
        ROOT_SHOPPING.innerHTML = '';
    }

    render() {
        const productsStore = localStorageUtil.getProducts();
        let htmlCatalog = '';
        let sumCatalog = 0;

        catalog.forEach(({ slug, name, price }) => {
            if (productsStore.indexOf(slug) !== -1) {
                htmlCatalog += `
                    <tr>
                        <td class="shopping_name"> ${name}</td>
                        <td class="shopping_price">${price.toLocaleString()} грн</td>
                    </tr>
                `;
                sumCatalog += price;
            }
        });

        const html = `
            <div class="shopping_container"> 
                <div class="shopping_basket">Корзина</div>
                <div class="shopping__close" onclick="shoppingPage.handlerClear();"></div>
                <table class="shopping_content">
                    ${htmlCatalog}
                    <tr>
                        <td class="shopping_name">💥 Сумма:</td>
                        <td class="shopping_price">${sumCatalog.toLocaleString()} грн</td>
                    </tr>
                </table>
            </div>
        `;

        ROOT_SHOPPING.innerHTML = html;
    }
};

const shoppingPage = new Shopping();
