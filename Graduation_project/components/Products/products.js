class Products {
  constructor() {
    this.classNameActive = 'content_btn_active';
    this.labelAdd = 'Добавить в корзину';
    this.labelRemove = 'Удалить из корзины';
  }

  handlerSetLocatStorage(element, id) {
    const { pushProduct, products } = localStorageUtil.putProducts(id);

    if (pushProduct) {
      element.classList.add(this.classNameActive);
      element.innerText = this.labelRemove;
    } else {
      element.classList.remove(this.classNameActive);
      element.innerText = this.labelAdd;
    }

    basketPage.render(products.length);
  }

  render() {
    const productsStore = localStorageUtil.getProducts()
    let activeClass = '';
    let htmlCatalog = '';

    catalog.forEach(({ slug, name, price, image }) => {
      let activeText = '';

      if (productsStore.indexOf(slug) === -1) {
        activeText = 'Добавить в корзину';
      } else {
        activeClass = ' ' + this.classNameActive;
        activeText = 'Удалить из корзины';
      }


      htmlCatalog += `
           
            <div class="line_1">
              <div class="content_img"><img  src="${image}"/> </div>
               <div>${name}</div>
               <div>${price} грн</div> 
               <button class="content_btn${activeClass}" onclick="productsPage.handlerSetLocatStorage(this, '${slug}')">${activeText}</button>           
            </div>
             `;
    });

    const html = `
        <div class="cards">
          ${htmlCatalog}
        </div>
         `;

    ROOT_PRODUCTS.innerHTML = html;
  }
};

const productsPage = new Products();
