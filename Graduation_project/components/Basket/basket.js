class Basket {
   
    handlerOpenShoppingPage() {
        shoppingPage.render();
    }

    render(count) {
        const html = `
           
                <div class="basket" onclick="basketPage.handlerOpenShoppingPage();">
                   
                 Корзина ( ${count} )
                    
                </div>
                   
            
        `;

        ROOT_BASKET.innerHTML = html;
    }
};

const basketPage = new Basket(); 

const productsStore =  localStorageUtil.getProducts();

basketPage.render(productsStore.length);

