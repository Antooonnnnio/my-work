document.addEventListener('DOMContentLoaded', () => {

    const ajaxSend = (formData) => {
        $(function () {

            var dat = JSON.stringify(formData);

            console.log(formData);

            $.ajax({
                url: "https://chandelier-shop.herokuapp.com/api/feedback/",
                type: "POST",

                data: dat,
                success: function () { },
                dataType: "json",
                contentType: "application/json"
            });
        })
    };

    const forms = document.getElementsByTagName('form');
    for (let i = 0; i < forms.length; i++) {
        forms[i].addEventListener('submit', function (e) {
            e.preventDefault();

            let formData = new FormData(this);
            formData = Object.fromEntries(formData);

            ajaxSend(formData)

            $('form[name=myform]').trigger('reset'); 
            document.getElementById('myform').value='';
        });
    };

});

