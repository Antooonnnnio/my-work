$('document').ready(function(){
    loadGoods();
 });
 
 function loadGoods() {
   $.getJSON('https://chandelier-shop.herokuapp.com/api/potolochnye-svetilniki/', function(data){
     
     let out = '';
     data.forEach(({ slug, name, price, image }) => {
       
       out += `
            
             <div class="line_1">
             <div class="content_img"><img  src="${image}"/> </div>
                <div>${name}</div>
                <div>${price} грн</div> 
                <button class="content_btn" onclick="productsPage.handlerSetLocatStorage(this, '${slug}')">Посмотреть товар</button>           
             </div>
              `;
     });
 
     const html = `
         <div class="cards">
           ${out}
         </div>
          `;
     $('#bra').html(html);
   })
 }