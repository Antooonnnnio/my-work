function render() {
    const productsStore = localStorageUtil.getProducts();

	basketPage.render(productsStore.length);

   productsPage.render();
}

let catalog = [];

fetch('https://chandelier-shop.herokuapp.com/api/products_list/')
	.then(res => res.json())
	.then(body => {
		catalog = body;

		render();

	})
