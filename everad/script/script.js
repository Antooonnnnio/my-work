// Секундомер для формы

class Timer {
	constructor(el, time) {
	  this.el = el;
	  this.time = time;
	  this.interval;
	  this.start();
	  this.render();
	}
	start() {
	  this.interval = setInterval(() => this.tick(), 1000);
	  }
	
	tick() {
	  this.time--;
	  this.render(); 
	}
	render() {
	  let h = parseInt(this.time / 3600);
	  let hs = this.time % 3600;
	  let m = parseInt(hs / 60);
	  let s = hs % 60;
	  this.el.innerHTML = `<div>${h} : ${m} : ${s}</div>`
	}
  }
  
  let el1 = document.querySelector('.time');
  let t1 = new Timer(el1, 7200);



// Дата для формы

  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); 
  var yyyy = today.getFullYear();

  today = 'Order date:' + dd + '.' + mm + '.' + yyyy;

  document.getElementById('data').innerHTML = today.toString();

 



 