const buttonPaint = document.querySelector(".btn")
buttonPaint.onclick = () => {
    let diametr = parseInt(prompt('Укажите диаметр круга'));
    if (diametr > 0) {
        for (let i = 0; i < 100; i++) {
            let around = document.querySelector(".container");
            let circle = document.createElement("input");
            const color = ["orange", "green", "blue", "red", "pink", "yellow", "wheat", "plum", "cyan", "brown"];
            let randomColor = color[Math.floor(Math.random() * 10)];
            circle.setAttribute("type", "button");
            circle.setAttribute("value", `    `);
            circle.setAttribute("onclick", "deleteSelf(this)");
            circle.style.backgroundColor = `${randomColor}`;
            circle.style.padding = `${diametr}px`;
            circle.classList.add("circles")
            around.append(circle);
        };
    }
};
const deleteSelf = (button) => {
    button.remove();

};