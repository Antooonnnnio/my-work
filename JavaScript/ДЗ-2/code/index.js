/*С помощью циклов нарисовать: 1) Прямоугольник 2) Прямоугольный треугольник 3) Равносторонный треугольник 4) Ромб*/

document.write("Прямоугольник" + "<br/>" + "<br/>");

for (let i = 0; i < 7; i++) {
    for (let q = 0; q < 30; q++) {
        document.write("*&nbsp;");
    }
    document.write("<br/>");
}
document.write("<br/>" + "<br/>");

document.write("Прямоугольный треугольник" + "<br/>");

for (let r = 0; r < 15; r++) {
    for (let t = 0; t < r; t++) {
        document.write("*&nbsp;");
    }
    document.write("<br/>");
};
document.write("<br/>" + "<br/>");

document.write("Равносторонний треугольник" + "<br/>" + "<br/>");

for (let i = 1; i < 15; i++) {
    for (let b = 15; b > i; b--) {
        document.write("&nbsp;&nbsp;");
    }
    for (let h = 0; h < i; h++) {
        document.write("*&nbsp;&nbsp;");
    }
    document.write("<br/>");
}
document.write("<br/>" + "<br/>");

document.write("Ромб" + "<br/>" + "<br/>");

for (let i = 1; i < 15; i++) {
    for (let b = 15; b > i; b--) {
        document.write("&nbsp;&nbsp;");
    }
    for (let h = 0; h < i; h++) {
        document.write("*&nbsp;&nbsp;");
    }
    document.write("<br/>");
};

for (let i = 1; i < 15; i++) {
    for (let b = 0; b < i; b++) {
        document.write("&nbsp;&nbsp;");
    }
    for (let h = 15; h > i; h--) {
        document.write("*&nbsp;&nbsp;");
    }
    document.write("<br/>");
};